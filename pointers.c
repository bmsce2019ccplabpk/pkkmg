#include<stdio.h>
void swap(int *a,int *b);
int main()
{
    int a,b;
    printf("Enter two numbers\n");
    scanf("%d%d",&a,&b);
    printf("Before swaping number_1=%d,number_2=%d\n",a,b);
    swap(&a,&b);
    printf("After swaping number_1=%d,number_2=%d\n",a,b);
    return 0;
}
void swap(int *a,int *b)
{
    int tmp;
    tmp=*a;
    *a=*b;
    *b=tmp;
}